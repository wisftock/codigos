<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CodigoQrController extends Controller
{
    public function index(){
        QrCode::size(800) -> generate('codigo QR realizado, exito!!!', '../public/qrcodes/qrcode.svg');
    }
}
